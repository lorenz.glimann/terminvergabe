"use strict";

const bodyParser = require("body-parser");
const express = require("express");
const session = require("express-session");
const lti = require("./lti");

const port = process.env.PORT || 3001;
// this express server should be secured/hardened for production use
const app = express();

const loki = require("lokijs");
let db = new loki("database.json", {
    autoload: true,
    autosave: true,
    autosaveInterval: 4000
});
let schedule = db.addCollection("schedule");
schedule.insert([
    {
        weekNumber: 1,
        amount: 3,
        duration: 1800000,
        beginTime: new Date("2019-12-12, 18:00:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 1,
        amount: 3,
        duration: 1800000,
        beginTime: new Date("2019-12-12, 18:20:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 1,
        amount: 3,
        duration: 1800000,
        beginTime: new Date("2019-12-12, 18:40:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 1,
        amount: 3,
        duration: 1800000,
        beginTime: new Date("2019-12-12, 19:00:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 1,
        amount: 3,
        duration: 1800000,
        beginTime: new Date("2019-12-12, 19:20:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 1,
        amount: 3,
        duration: 1800000,
        beginTime: new Date("2019-12-12, 19:40:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 2,
        amount: 5,
        duration: 1800000,
        beginTime: new Date("2019-12-13, 18:00:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 2,
        amount: 5,
        duration: 1800000,
        beginTime: new Date("2019-12-13, 18:20:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 2,
        amount: 5,
        duration: 1800000,
        beginTime: new Date("2019-12-13, 18:40:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 2,
        amount: 5,
        duration: 1800000,
        beginTime: new Date("2019-12-13, 19:00:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 2,
        amount: 5,
        duration: 1800000,
        beginTime: new Date("2019-12-13, 19:20:00").getTime() - 600000,
        appointments: []
    },
    {
        weekNumber: 2,
        amount: 5,
        duration: 1800000,
        beginTime: new Date("2019-12-13, 19:40:00").getTime() - 600000,
        appointments: []
    },
]);

app.set("view engine", "pug");
// memory store shouldn't be used in production
app.use(session({
    secret: process.env.SESSION_SECRET || "dev",
    cookie: {httpOnly: false},
    resave: false,
    saveUninitialized: true,
}));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.enable("trust proxy");

const debug = true;
const debugsession = {
    username: "testuser",
    userId: 42,
    isTutor: true
};

app.get("/admin", (req, res, next) => {
    let session = req.session;
    if (!session.userId) {
        if (debug) {
            session = debugsession;
        } else {
            next(new Error("Session invalid. Please login via LTI to use this application."));
        }
    }
    if (!session.isTutor) {
        res.sendStatus(403);
        return;
    }

    return res.render("admin", {
        email: session.email,
        username: session.username,
        ltiConsumer: session.ltiConsumer,
        userId: session.userId,
        isTutor: session.isTutor,
        context_id: session.context_id,
        termine: schedule.find({beginTime: {$gte: Date.now()}}).sort((termin1, termin2) => {
            return termin1.weekNumber - termin2.weekNumber || termin2.beginTime - termin1.beginTime;
        }),
    });
});

app.get("/tutor", (req, res, next) => {
    let session = req.session;
    if (!session.userId) {
        if (debug) {
            session = debugsession;
        } else {
            next(new Error("Session invalid. Please login via LTI to use this application."));
        }
    }
    if (!session.isTutor) {
        res.sendStatus(403);
        return;
    }

    return res.render("tutor", {
        email: session.email,
        username: session.username,
        ltiConsumer: session.ltiConsumer,
        userId: session.userId,
        isTutor: session.isTutor,
        context_id: session.context_id,
        termine: schedule.find({beginTime: {$gte: Date.now()}}).sort((termin1, termin2) => {
            return termin1.weekNumber - termin2.weekNumber || termin2.beginTime - termin1.beginTime;
        }),
    });
});

app.get("/book", (req, res, next) => {
    let session = req.session;
    if (!session.userId) {
        if (debug) {
            session = debugsession;
        } else {
            next(new Error("Session invalid. Please login via LTI to use this application."));
        }
    }

    return res.render("index", {
        email: session.email,
        username: session.username,
        ltiConsumer: session.ltiConsumer,
        userId: session.userId,
        isTutor: session.isTutor,
        context_id: session.context_id,
        termine: schedule.find({beginTime: {$gte: Date.now()}}).sort((termin1, termin2) => {
            return termin1.weekNumber - termin2.weekNumber || termin2.beginTime - termin1.beginTime;
        }),
        bookedWeeks: schedule.find({appointments: {$contains: session.username}}).map(slot => slot.weekNumber)
    });
});

app.post("/launch_lti", lti.handleLaunch);

app.get("/api/bookings", function (req, res, next) {
    let session = req.session;
    if (!session.userId) {
        if (debug) {
            session = debugsession;
        } else {
            next(new Error("Session invalid. Please login via LTI to use this application."));
        }
    }
    let bookings = schedule.find().filter(slot => slot.appointments.includes(session.username));
    res.send(bookings);
});
let repl = require("repl").start();
repl.context.db = db;
repl.context.schedule = schedule;

app.get("/api/book", function (req, res, next) {
    let session = req.session;
    if (!session.userId) {
        if (debug) {
            session = debugsession;
        } else {
            next(new Error("Session invalid. Please login via LTI to use this application."));
        }
    }
    const slotID = parseInt(req.query.slotID);
    console.log(slotID);
    if (typeof slotID !== "number") {
        res.sendStatus(400);
        return;
    }

    let slot = schedule.findOne({beginTime: slotID});
    if (schedule.find({weekNumber: slot.weekNumber, appointments: {$contains: session.username}}).length > 0) {
        return next(new Error("You already have an appointment for that week/exercise sheet. You need to unbook that first if you want make another."));
    }
    slot.appointments.push(session.username);
    schedule.update(slot);

    res.redirect("/book");
});

app.get("/api/unbook", function (req, res, next) {
    let session = req.session;
    if (!session.userId) {
        if (debug) {
            session = debugsession;
        } else {
            next(new Error("Session invalid. Please login via LTI to use this application."));
        }
    }
    const slotID = parseInt(req.query.slotID);
    console.log(slotID);
    if (typeof slotID !== "number") {
        res.sendStatus(400);
        return;
    }

    let slot = schedule.findOne({beginTime: slotID});
    slot.appointments = slot.appointments.filter(a => a !== session.username);
    schedule.update(slot);
    res.redirect("/book");
});

app.post("/api/addToSchedule", function (req, res, next) {
    let session = req.session;
    if (!session.userId) {
        if (debug) {
            session = debugsession;
        } else {
            next(new Error("Session invalid. Please login via LTI to use this application."));
        }
    }
    console.log(`got body:\n`, req.body);
    schedule.insert(req.body);
    res.sendStatus(200);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
